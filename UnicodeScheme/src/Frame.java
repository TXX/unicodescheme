import java.awt.TextArea;

import javax.swing.JFrame;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class Frame extends JFrame {
	
	public static void main(String[] args) {
		Frame myFrame = new Frame();
		myFrame.setVisible(true);	
	}
	
	public Frame() {
		super("Herror GUI!");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		char a = '\u0001';
		JPanel myPanel = new JPanel();
		TextArea myLabel = new TextArea();
		String b = "";
		for(int i = -1; i <= 1000; i++) {
			b += a;
			b += ("\t: " + i + "\n");
			a += 1;
			myLabel.setText(b);
		}
		myPanel.add(myLabel);
		
		this.setLocationRelativeTo(null);
		this.add(myPanel);
		this.pack();
	}

}
